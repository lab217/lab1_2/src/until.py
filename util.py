import timeit
import random


def create_dict(lst):
    result_dict = {}
    for index, num in lst:
        result_dict.setdefault(num, [])
        result_dict[num].append(index)
    return result_dict


def check_input(data):
    data = data.replace(' ', '')
    if data.isdigit():
        return True
    else:
        return False


def forming_list(data):
    data = data.split(' ')
    lst = []
    for index in range(len(data)):
        lst.append((index + 1, int(data[index])))
    return lst


def swap():
    return None


def bubble_sort(lst, reverse=False):
    swapped = True
    if reverse:
        compare = lambda first, second: first < second
    else:
        compare = lambda first, second: first > second
    while swapped:
        swapped = False
        for i in range(len(lst) - 1):
            if compare(lst[i][1], lst[i + 1][1]):
                swap()
                lst[i], lst[i + 1] = lst[i + 1], lst[i]
                swapped = True
    return lst


def insert_sort(lst, reverse=False):
    if reverse:
        compare = lambda first, second: first < second
    else:
        compare = lambda first, second: first > second
    for i in range(1, len(lst)):
        swap()
        j = i - 1
        key = lst[i]
        while compare(lst[j][1], key[1]) and j >= 0:
            swap()
            lst[j + 1] = lst[j]
            j -= 1
        swap()
        lst[j + 1] = key
    return lst


# def shell_sort(lst, reverse=False):
#     if reverse:
#         compare = lambda first, second: first < second
#     else:
#         compare = lambda first, second: first > second
#     step = len(lst) // 2
#     while step:
#         for i, el in enumerate(lst):
#             while i >= step and compare(lst[i - step], el):
#                 lst[i] = lst[i - step]
#                 i -= step
#             lst[i] = el
#         step = 1 if step == 2 else int(step * 5.0 / 11)
#     return lst


def shell_sort(result, reverse=False):
    if reverse:
        compare = lambda first, second: first < second
    else:
        compare = lambda first, second: first > second
    t = len(result) // 2
    while t > 0:
        swap()
        for i in range(len(result) - t):
            j = i
            while j >= 0 and compare(result[j][1], result[j + t][1]):
                swap()
                result[j], result[j + t] = result[j + t], result[j]
                j -= 1
                swap()
        t = t // 2
    return result


# def gap_insertion_sort(a_list, start, gap, key = lambda x: x[1]):
#     for i in range(start + gap, len(a_list), gap):
#         current_value = a_list[i]
#         position = i
#         while position >= gap and key(a_list[position - gap]) > key(current_value):
#             a_list[position] = a_list[position - gap]
#             position = position - gap
#         a_list[position] = current_value
#
#
# def shell_sort(lst, reverse=False):
#     """Shell Sort:
#         Args:
#             lst (list): list of random numbers.
#         Returns:
#             elapsed (float): Function exec time.
#        """
#     sublist_count = len(lst) // 2
#     while sublist_count > 0:
#         for start_position in range(sublist_count):
#             gap_insertion_sort(lst, start_position, sublist_count)
#
#         sublist_count = sublist_count // 2
#     return lst
listos = [(1, random.randint(1, 10000)) for i in range(1, 100)]
g = listos
start = timeit.default_timer()


def partition(lst, l, h):
    i = (l - 1)
    x = lst[h][1]

    for j in range(l, h):
        if lst[j][1] <= x:
            i = i + 1
            swap()
            lst[i], lst[j] = lst[j], lst[i]
    swap()
    lst[i + 1], lst[h] = lst[h], lst[i + 1]
    return i + 1


def quick_sort(lst, reverse=False):
    h = len(lst) - 1
    l = 0
    size = h - l + 1
    stack = [0] * size

    top = -1

    top = top + 1
    stack[top] = l
    top = top + 1
    stack[top] = h

    while top >= 0:

        h = stack[top]
        top = top - 1
        l = stack[top]
        top = top - 1

        p = partition(lst, l, h)

        if p - 1 > l:
            top = top + 1
            stack[top] = l
            top = top + 1
            stack[top] = p - 1

        if p + 1 < h:
            top = top + 1
            stack[top] = p + 1
            top = top + 1
            stack[top] = h

    if reverse:
        return lst[::-1]
    else:
        return lst


def selection_sort(array, reverse=False):
    for i in range(len(array) - 1):
        m = i
        j = i + 1
        while j < len(array):
            swap()
            if array[j] < array[m]:
                m = j
            swap()
            j = j + 1
        array[i], array[m] = array[m], array[i]
        return array



def introsort(lst, reverse=False):
    maxdepth = (len(lst).bit_length() - 1) * 2
    introsort_helper(lst, 0, len(lst), maxdepth)
    return lst

def introsort_helper(lst, start, end, maxdepth):
    if end - start <= 1:
        return
    elif maxdepth == 0:
        heapsort(lst, start, end)
    else:
        p = partitions(lst, start, end)
        introsort_helper(lst, start, p + 1, maxdepth - 1)
        introsort_helper(lst, p + 1, end, maxdepth - 1)


def partitions(lst, start, end):
    pivot = lst[start]
    i = start - 1
    j = end

    while True:
        i = i + 1
        while lst[i] < pivot:
            swap()
            i = i + 1
        j = j - 1
        while lst[j] > pivot:
            j = j - 1

        if i >= j:
            return j

        swaps(lst, i, j)


def swaps(lst, i, j):
    lst[i], lst[j] = lst[j], lst[i]


def heapsort(lst, start, end):
    build_max_heap(lst, start, end)
    for i in range(end - 1, start, -1):
        swap()
        swaps(lst, start, i)
        max_heapify(lst, index=0, start=start, end=i)


def build_max_heap(lst, start, end):
    swap()
    def parent(i):
        swap()
        return (i - 1) // 2

    length = end - start
    index = parent(length - 1)
    while index >= 0:
        swap()
        max_heapify(lst, index, start, end)
        index = index - 1


def max_heapify(lst, index, start, end):
    def left(i):
        swap()
        return 2 * i + 1

    def right(i):
        swap()
        return 2 * i + 2

    size = end - start
    l = left(index)
    r = right(index)
    if (l < size and lst[start + l] > lst[start + index]):
        swap()
        largest = l
    else:
        swap()
        largest = index
    if (r < size and lst[start + r] > lst[start + largest]):
        swap()
        largest = r
    if largest != index:
        swap()
        swaps(lst, start + largest, start + index)
        max_heapify(lst, largest, start, end)
    return lst
# quick_sort(g)
# stop = timeit.default_timer()
# print(stop - start)
